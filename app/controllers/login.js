import Ember from 'ember';

export default Ember.Controller.extend({
  email: "",
  password: "",
  auth: Ember.inject.service(),
  noErrors: true,
  actions: {
    login() {
      const email = this.get('email').trim();
      const password = this.get('password').trim();
      this.get('store').query('user', {
        filter: {
          "user.email": email,
          "user.password": password
        }
      }).then((users) => {
        const hasUser = users.get('length') > 0;
        const auth = this.get('auth');
        auth.set('loggedIn', hasUser);
        this.set('noErrors', hasUser);
        if(hasUser) {
          if(auth.get('previousTransition') !== null) {
            const previousTransition = auth.get('previousTransition');
            auth.set('previousTransition', null);
            previousTransition.retry();
          } else {
            this.transitionToRoute('index');
          }
        }

      });
    }
  }
});
