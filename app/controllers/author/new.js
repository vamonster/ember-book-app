import Ember from 'ember';

export default Ember.Controller.extend({
    name: '',
    disabledProperty: Ember.computed('name', function() {
        if(this.get('name').trim() === '') {
            return 'true';
        }
        return 'false';
    }),
    actions: {
        addNewRecord() {
            const name = this.get('name').trim();
            if (name === '') {
                return;
            }
            const record = this.get('store').createRecord('author', {
                name: name
            });
            record.save().then((author) => {
              this.transitionToRoute('author.show', author);
            });
        }
    }
});
