import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    deleteAuthor() {
      const author = this.get('model');
      author.destroyRecord().then(()=>{
        this.transitionToRoute('author.index');
      });
    },
    addRandomBook() {
      const author = this.get('model');
      const book = this.get('store').createRecord('book', {
        title: "New book",
        genre: "Science Fiction",
        language: "C"
      });
      author.get('books').pushObject(book);
      book.save().then(()=>{
        author.save();
      });
    }
  }
});
