import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('author', function() {
    this.route('show', { path: '/:author_id' });
    this.route('new');
    this.route('edit', { path: 'edit/:author_id' });
  });
  this.route('login');
});

export default Router;
