import DS from 'ember-data';

export default DS.JSONAPISerializer.extend({
  serialize(snapshot, options) {
    const json = this._super(snapshot, options);
    // FIX ME PLEASE!!!
    json.data.type = snapshot.modelName;
    const relationships = json.data.relationships;
    if(relationships) {
      Object.keys(relationships).forEach((relativeName) => {
        const relative = relationships[relativeName];
        relative.data.forEach((relativeObject) => {
          // THIS IS MORE WRONG THAN PREVIOUS ONE
          relativeObject.type = relativeName.substring(0, relativeName.length - 1);
        });
      });
    }
    return json;
  }
});
