import Ember from 'ember';

export default Ember.Route.extend({
    auth: Ember.inject.service(),
    beforeModel(transision) {
        if(!this.get('auth').get('loggedIn')) {
            this.get('auth').set('previousTransition', transision);
            this.transitionTo('login');
        }
    }
});
